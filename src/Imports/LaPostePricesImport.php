<?php

namespace DFM\Shipping\Imports;

use DFM\Shipping\Models\LaPostePrice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class LaPostePricesImport implements ToCollection
{
    use Importable;

    /**
     * @param  Collection  $rows
     */
    public function collection(Collection $rows)
    {
        LaPostePrice::truncate();

        foreach ($rows as $key => $row) {
            if ($key == 0) {
                continue;
            }

            LaPostePrice::create([
                'weight' => (double) $row[0],
                'price'  => (double) $row[1],
            ]);
        }
    }
}
