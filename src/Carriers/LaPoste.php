<?php

namespace DFM\Shipping\Carriers;

use DFM\Shipping\Models\LaPostePrice;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Models\CartShippingRate;

class LaPoste extends AbstractShipping
{
    /**
     * Shipment method code
     *
     * @var string
     */
    protected $code  = 'la-poste';

    /**
     * @return false|CartShippingRate
     */
    public function calculate()
    {
        if (! $this->isAvailable()) {
            return false;
        }

        $cart = Cart::getCart();
        $code = $this->getCartCarrier($cart);

        if (! in_array($code, ['', $this->code])) {
            return false;
        }

        $cartWeight = $cart->items->sum('total_weight');

        if ($cartWeight > 30) {
            return false;
        }

        if (! ($laPostePrice = LaPostePrice::where('weight', '>=', $cartWeight)->orderBy('price')->first())) {
            return false;
        }

        $object = new CartShippingRate();

        $object->carrier = 'la-poste';
        $object->carrier_title = $this->getConfigData('title');
        $object->method = 'la-poste_la-poste';
        $object->method_title = $this->getConfigData('title');
        $object->method_description = $this->getConfigData('description');
        $object->price = $laPostePrice->price;
        $object->base_price = $laPostePrice->price;

        return $object;
    }
}
