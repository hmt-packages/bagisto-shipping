<?php

namespace DFM\Shipping\Carriers;

use DFM\Shipping\Models\CoupePrice;
use Webkul\Checkout\Facades\Cart;
use Webkul\Checkout\Models\CartShippingRate;
use Webkul\Core\Models\CountryState;

class Coupe extends AbstractShipping
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code = 'coupe';

    /**
     * @return false|CartShippingRate
     */
    public function calculate()
    {
        if (! $this->isAvailable()) {
            return false;
        }

        $cart = Cart::getCart();
        $code = $this->getCartCarrier($cart);

        if (! in_array($code, ['', 'la-poste', $this->code])) {
            return false;
        }

        $cartWeight = $cart->items->sum('total_weight');
        $shippingAddress = $cart->shipping_address;

        if ($cartWeight > 100 ||
            ($countryCode = $shippingAddress->country) != 'FR' ||
            ! ($stateCode = $shippingAddress->state) ||
            ! ($state = CountryState::where([
                ['country_code', '=', $countryCode],
                ['code', '=', $stateCode]
            ])->first()) ||
            ! ($coupePrice = CoupePrice::where([
                ['weight', '>=', $cartWeight],
                ['state_id', '=', $state->id]
            ])->orderBy('price')->first())
        ) {
            return false;
        }

        $object = new CartShippingRate();

        $object->carrier = 'coupe';
        $object->carrier_title = $this->getConfigData('title');
        $object->method = 'coupe_coupe';
        $object->method_title = $this->getConfigData('title');
        $object->method_description = $this->getConfigData('description');
        $object->price = $coupePrice->price;
        $object->base_price = $coupePrice->price;

        return $object;
    }
}
