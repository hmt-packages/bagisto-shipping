<?php

namespace DFM\Shipping\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DFM\Shipping\DataGrids\CoupeDataGrid;

class CoupeController extends Controller
{
    /**
     * Display a listing of the coupe.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dataGrid = app(CoupeDataGrid::class);

        return view('dfm-shipping::admin.index', compact('dataGrid'));
    }
}
