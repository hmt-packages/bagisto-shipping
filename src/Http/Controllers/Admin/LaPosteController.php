<?php

namespace DFM\Shipping\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DFM\Shipping\DataGrids\LaPosteDataGrid;

class LaPosteController extends Controller
{
    /**
     * Display a listing of the post office.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $dataGrid = app(LaPosteDataGrid::class);

        return view('dfm-shipping::admin.index', compact('dataGrid'));
    }
}
