<?php

namespace DFM\Shipping\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LaPostePrice
 *
 * @package DFM\Shipping
 *
 * @property-read  int     $id
 * @property-read  double  $weight
 * @property-read  double  $price
 */
class LaPostePrice extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'la_poste_prices';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['weight', 'price'];
}
